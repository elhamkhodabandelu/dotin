package com.UserPAckTest;

import com.optionalExample.ImmplementUserService;
import com.optionalExample.User;
import com.optionalExample.UserService;
import org.junit.Test;

import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class UserTest {
    @Test
    public void success_get_user_by_id() {
        UserService  userService=new ImmplementUserService();
        User user = userService.getUserById(110);
        assertThat(user.getName(), equalTo("a"));
    }
}
