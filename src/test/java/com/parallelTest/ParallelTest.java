package com.parallelTest;

import com.ParallelExample.ParallelSum;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ParallelTest {
    @Test
    public void success_parallel_sum() {
        Integer sum = ParallelSum.sum();

        assertThat(sum, equalTo(15));
    }

    @Test
    public void success_iterate() {

        long size = ParallelSum.itearate();
        assertThat(size, equalTo(4L));
    }

}
