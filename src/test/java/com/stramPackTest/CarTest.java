package com.stramPackTest;

import com.streamPack.Car;
import com.streamPack.CarService;
import com.streamPack.Color;
import com.streamPack.ImplementationCarService;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class CarTest {


    private CarService carService = new ImplementationCarService(); // TODO: 4/30/2018 new an implemented service
    private List<Car> carList;

    @Before
    public void initTest() {
        carList = TestDataUtil.generateSampleCar();


    }

    @Test
    public void success_filter_black_car_count() {
        Stream<Car> stream = carService.filterCar(carList, car -> car.getColor().equals(Color.BLACK));
        long count = stream.count();
        assertThat(count, equalTo(2L));
    }

    @Test
    public void success_total_price_of_car() {
        Integer totalPrice = carService.calcuteSumOfPrice(carList);
        assertThat(totalPrice, equalTo(135));
    }

    @Test
    public void success_collect_to_list() {
        List<Car> cars = carService.collectToList(carList.stream().filter(car -> car.getPrice()>11));
        assertThat(cars.size(),equalTo(5));
    }

    @Test
    public void success_collect_to_map() {
        Map<Integer, Car> colorCarMap= carService.collectToMap(carList.stream().filter(car -> car.getPrice()>5 && car.getColor().equals(Color.BLACK)));
        assertThat(colorCarMap.size(),equalTo(3));
    }

    @Test
    public void success_match() {
       Integer result = carService.match(carList.stream(),car -> car.getColor().equals(Color.BLACK));
        assertThat(result,equalTo(1));
    }
}
