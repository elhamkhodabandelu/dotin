package com.stramPackTest;

import com.streamPack.Car;
import com.streamPack.Color;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class TestDataUtil {
    public static List<Car> generateSampleCar() {
        BiFunction<Color, Integer, Car> carBiFunction = Car::new;
        return Arrays.asList(
                carBiFunction.apply(Color.GREEN, 10)
                , carBiFunction.apply(Color.BLACK, 11)
                , carBiFunction.apply(Color.RED, 7)
                , carBiFunction.apply(Color.YELLOW, 12)
                , carBiFunction.apply(Color.YELLOW, 13)
                , carBiFunction.apply(Color.GREEN, 15)
                , carBiFunction.apply(Color.BLACK, 5)
                , carBiFunction.apply(Color.BLACK, 35)
                , carBiFunction.apply(Color.BLACK, 25)
                , carBiFunction.apply(Color.BLACK, 2)
        );
    }
}
