package com.appleExample;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class AppleTest {

    private AppleService appleService = new ImplementationAppleService(); // TODO: 4/30/2018 new an implemented service
    private List<Apple> apples;

    @Before
    public void initTest() {
        apples = TestDataUtil.generateSampleApple();
    }

    @Test
    public void success_filter_yellow_apple_list() {
        assertThat(appleService.filter(apples, a -> a.getColor() == Color.YELLOW).size()
                , equalTo(2));
    }
}
