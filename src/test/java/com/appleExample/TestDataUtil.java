package com.appleExample;

import java.util.Arrays;
import java.util.List;

public class TestDataUtil {

    public static List<Apple> generateSampleApple(){
        return Arrays.asList(
            new Apple(Color.GREEN, 10)
            ,new Apple(Color.GREEN, 11)
            ,new Apple(Color.RED, 7)
            ,new Apple(Color.YELLOW, 10)
            ,new Apple(Color.YELLOW, 10)
            ,new Apple(Color.GREEN, 15)
            ,new Apple(Color.GREEN, 5)
        );
    }
}
