package com.countWordTest;

import com.countWord.File;

import java.util.List;

@FunctionalInterface
public interface ReturnFileList {

    List<File> getFileList();
}
