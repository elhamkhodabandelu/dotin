package com.countWordTest;

import com.countWord.File;
import com.countWord.ImplementationWordCountService;
import com.countWord.WordCountService;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.function.Function;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class WordCountTest {

    private WordCountService wordCountService = new ImplementationWordCountService(); // TODO: 4/30/2018 new an implemented service
    //private List<File> fileList;
    ReturnFileList fileList;

    @Before
    public void initTest() {
        //fileList = TestDataUtil.generateSampleWordcount();
        fileList = TestDataUtil::generateSampleWordcount;


    }

    @Test
    public void success_word_count() {
        //Integer wordcounting = wordCountService.wordcounting(fileList);
        Function<List<File>, Integer> function = wordCountService::wordcounting;
        Integer wordcounting = function.apply(fileList.getFileList());
        assertThat(wordcounting, equalTo(210));
    }
}