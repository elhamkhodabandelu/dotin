package com.flatMapTest;

import com.flatMapExample.Developer;
import com.flatMapExample.FlatMapService;
import com.flatMapExample.ImmplementFlatMapService;
import com.flatMapExample.TestDataUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class FlatMapTest {

    private FlatMapService flatMapServiceervice = new ImmplementFlatMapService();
    private List<String> List;


    @Test
    public void success_flat_map() {
//        List = flatMapServiceervice.flatMap();
//        assertThat(List.size(), equalTo(4));

        Optional.empty().map(i -> {
            System.out.println("number is " + i);
            return i;
        }).orElseGet(() ->{
            System.out.println("number is emty");
            return null;
        });
    }
}
