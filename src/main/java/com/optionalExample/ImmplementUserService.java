package com.optionalExample;

        import java.util.List;
        import java.util.Optional;
        import java.util.stream.Collectors;

        import static com.sun.deploy.trace.Trace.print;

public class ImmplementUserService implements UserService {


    @Override
    public User getUserById(Integer id) {
        List<User> userList = TestDataUtil.generateSampleUser();
        Optional<User> optionalUser = userList.stream().filter(a -> a.getId().equals(id)).findFirst();
        optionalUser.ifPresent(user -> System.out.print(user.getName()+"/"+user.getId()));
        return optionalUser.get();
    }
}
