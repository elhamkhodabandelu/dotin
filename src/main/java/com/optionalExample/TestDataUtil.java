package com.optionalExample;

import com.optionalExample.User;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;

public class TestDataUtil {
    public static List<User> generateSampleUser() {
        BiFunction<String, Integer, User> userBiFunction = User::new;
        return Arrays.asList(
                userBiFunction.apply("a", 110)
                , userBiFunction.apply("b", 111)
                , userBiFunction.apply("c", 17)
                , userBiFunction.apply("d", 112)
                , userBiFunction.apply("e", 113)
                , userBiFunction.apply("f", 115)
                , userBiFunction.apply("g", 15)


        );
    }
}
