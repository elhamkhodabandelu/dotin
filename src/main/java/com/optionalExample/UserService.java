package com.optionalExample;

import java.util.Optional;

public interface UserService {
    User getUserById(Integer id);
}
