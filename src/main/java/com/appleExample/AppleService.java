package com.appleExample;

import java.util.List;
import java.util.function.Predicate;

public interface AppleService {
    List<Apple> filter(List<Apple> apples, Predicate<Apple> predicate);
}


