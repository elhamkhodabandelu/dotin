package com.appleExample;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class ImplementationAppleService implements AppleService {

    @Override
    public List<Apple> filter(List<Apple> apples, Predicate<Apple> predicate) {
        List<Apple> result = new ArrayList<>();
        for (Apple apple : apples) {
            if (predicate.test(apple)) {
                result.add(apple);
            }
        }
        return result;
    }
}



