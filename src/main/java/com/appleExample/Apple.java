package com.appleExample;

public class Apple {

    private Color color;
    private double wieght;

    public Apple(Color color, double wieght) {
        this.color = color;
        this.wieght = wieght;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getWieght() {
        return wieght;
    }

    public void setWieght(double wieght) {
        this.wieght = wieght;
    }
}
