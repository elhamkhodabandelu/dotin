package com.ParallelExample;

public class Accumulator {
    Integer total = 0;

    public void add(Integer value) {
        total += value;
    }
}
