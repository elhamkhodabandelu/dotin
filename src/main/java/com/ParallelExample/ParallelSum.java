package com.ParallelExample;

import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class ParallelSum {
    public static Integer sum() {

        Accumulator accumulator = new Accumulator();
        IntStream longStream = IntStream.rangeClosed(1, 5);
        longStream.parallel().forEach(accumulator::add);
        return accumulator.total;
    }

    public static long itearate() {
        IntStream intStream = IntStream.rangeClosed(1, 4000).parallel().filter(a -> a % 1000 == 0);
        //intStream.forEach(System.out::println);
        return intStream.count();
    }

}
