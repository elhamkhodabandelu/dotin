package com.countWord;

public class File {
    private String path;

    public File() {

    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public File(String path) {
        this.path = path;
    }
}
