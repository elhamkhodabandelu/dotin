package com.countWord;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ImplementationWordCountService implements WordCountService {


    @Override
    public Integer wordcounting(List<File> fileList) {
        Integer result = 0;
        ExecutorService executor = Executors.newFixedThreadPool(3);
        // List<Callable<Integer>> callableTasklist = new ArrayList<>();//ask constructor method refrence

 /*       fileList.forEach(file -> {
            Callable<Integer> callableObj = () -> {
                Function<File, Integer> function = MethodRefrence::countingFile;
                return function.apply(file);
            };
            callableTasklist.add(callableObj);
        });*/

        List<CompletableFuture<Integer>> completableFutureList = new ArrayList<>();
        fileList.forEach(file -> {
            CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
                        Function<File, Integer> function = MethodRefrence::countingFile;
                        return function.apply(file);
                    }
                    , executor);
            completableFutureList.add(completableFuture);
        });
        Integer reduce = completableFutureList.stream().map(CompletableFuture::join).reduce(0, Integer::sum);
        return reduce;








/*        for (File file : pathList) {
            Callable<Integer> callableObj = new Callable<Integer>() {
                MethodRefrence methodRefrence = new MethodRefrence();

                @Override
                public Integer call() throws NullPointerException{

                    Function<File, Integer> function = methodRefrence::countingFile;
                        Integer result = function.apply(file);
                        return result;
                }

            };
            callableTasklist.add(callableObj);
        }*/


        /*List<Future<Integer>> futureList = null;
        try {
            futureList = executor.invokeAll(callableTasklist);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (Future<Integer> future : futureList) {
            Integer res = 0;
            try {
                res = future.get(1, TimeUnit.SECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                e.printStackTrace();
            }
            result += res;
            System.out.println(result);
        }
        return result;
    }*/

/*

        for (Callable callableObj:callableTasklist) {
            Future<Integer> future = executor.submit(callableObj);
            Integer r = future.get(1,TimeUnit.SECONDS);
            result=result+r;
            System.out.println(result);
        }
        return result;
*/
    }
}

