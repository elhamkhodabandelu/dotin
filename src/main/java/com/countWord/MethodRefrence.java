package com.countWord;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MethodRefrence {
    public static Integer countingFile(File file) {
        int count = 0;
        try (Scanner sc = new Scanner(new FileInputStream(file.getPath()))) {
            while (sc.hasNext()) {
                sc.next();
                count++;
            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
        return count;

    }

}
