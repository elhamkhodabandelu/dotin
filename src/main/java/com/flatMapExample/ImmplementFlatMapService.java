package com.flatMapExample;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ImmplementFlatMapService implements FlatMapService {

    @Override
    public List<String> flatMap() {

        List<Developer> developerList = TestDataUtil.generateSampleDeveloper();
        Stream<String> stringStream = developerList.stream().map(developer -> developer.getLanguage()).flatMap(l -> l.stream());
        // stringStream.forEach(System.out::println);
        List<String> stringList = stringStream.collect(Collectors.toList());
        return stringList;
    }
}
