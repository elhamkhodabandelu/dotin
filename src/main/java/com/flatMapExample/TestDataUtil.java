package com.flatMapExample;

import com.appleExample.Apple;
import com.appleExample.Color;
import com.flatMapExample.Developer;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;

public class TestDataUtil {

    public static List<Developer> generateSampleDeveloper() {
        BiFunction<String, List<String>, Developer> developerBiFunction = Developer::new;

        return Arrays.asList(
                developerBiFunction.apply("elham", Arrays.asList("a", "b"))
                , developerBiFunction.apply("raha", Arrays.asList("c", "d"))

        );
    }
}