package com.flatMapExample;

import java.util.List;

public interface FlatMapService {
    List<String> flatMap();
}
