package com.flatMapExample;

import java.util.List;

public class Developer {
    private String name;
    private List<String> language;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getLanguage() {
        return language;
    }

    public void setLanguage(List<String> language) {
        this.language = language;
    }

    public Developer(String name, List<String> language) {
        this.name = name;
        this.language = language;
    }
}
