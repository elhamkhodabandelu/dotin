package com.streamPack;

import com.appleExample.Apple;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ImplementationCarService implements CarService {


    @Override
    public Stream<Car> filterCar(List<Car> cars, Predicate<Car> predicate) {
        Stream<Car> stream = cars.stream().filter(predicate).sorted(Comparator.comparing(Car::getPrice)).limit(2);
        stream.forEach(car -> System.out.println("Price:" + car.getPrice() + ", Color: " + car.getColor()));
        //Stream<Car> stream = cars.stream().filter(predicate).sorted(Comparator.comparing(Car::getPrice)).skip(2);
        return stream;
    }

    @Override
    public Integer calcuteSumOfPrice(List<Car> cars) {
        Optional<Integer> totalPrice = cars.stream().map(car -> car.getPrice()).reduce((price1, price2)->price1+price2);
        totalPrice.ifPresent(System.out::println);
        return totalPrice.get();
    }

    @Override
    public List<Car> collectToList(Stream<Car> carStream) {
        List<Car> cars = carStream.collect(Collectors.toList());
        cars.forEach(car -> System.out.println(car.getPrice()));
        return cars;
    }

    @Override
    public Map<Integer, Car> collectToMap(Stream<Car> carStream) {
        Map<Integer, Car> map = carStream.collect(Collectors.toMap(car -> car.getPrice(), car->car));
        for (Map.Entry<Integer, Car> entry : map.entrySet()){
            System.out.println(entry.getKey() + "/" +entry.getValue().getColor());
        }
        return map;
    }

    @Override
    public Integer match(Stream<Car> carStream,Predicate<Car> predicate) {
        Integer result = -1;
        if (carStream.anyMatch(predicate)){
            System.out.println( "At least one of the members is black");
            result=1;
        }
        else if (carStream.allMatch(predicate)) {
            System.out.println("all of the members is black");
            result=100;
        }
        else if(carStream.allMatch(predicate)){
            System.out.println("none of the members is black");
            result=0;
        }
        return result;
    }


}



