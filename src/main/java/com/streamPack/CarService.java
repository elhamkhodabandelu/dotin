package com.streamPack;

import com.appleExample.Apple;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

public interface CarService {
    Stream<Car> filterCar(List<Car> cars, Predicate<Car> predicate);
    Integer calcuteSumOfPrice(List<Car> cars);
    List<Car> collectToList(Stream<Car> carStream);
    Map<Integer,Car> collectToMap(Stream<Car> carStream);
    Integer match(Stream<Car> carStream,Predicate<Car> predicate);
}


