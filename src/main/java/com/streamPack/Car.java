package com.streamPack;

public class Car {

    private Color color;
    private Integer price;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Car(Color color, Integer price) {
        this.color = color;
        this.price = price;
    }
}
